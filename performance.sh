#!/bin/sh

VAR=$1

sar -P 0 $VAR > CPU0.txt &
sar -P 1 $VAR > CPU1.txt &
sar -P 2 $VAR > CPU2.txt &
sar -P 3 $VAR > CPU3.txt &

sar -r $VAR > memoria.txt &

sar -m CPU $VAR > CPUfreq.txt