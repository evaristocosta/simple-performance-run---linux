# Simple Performance Run - Linux

Just a simple script using "sar" to gather performance info during execution and saving the results to separated files. The info gathered is:
- CPU0;
- CPU1;
- CPU2;
- CPU3;
- CPU frequency;
- Memory used.